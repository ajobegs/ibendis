import 'package:flutter/material.dart';

class UserDetails extends StatelessWidget {

  final String nama;
  final String gambar;

  const UserDetails({Key key, this.nama, this.gambar}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("User Details Info. # $nama"),
          backgroundColor: Colors.purple,
        ),
        body: Container(
          child: Center(
            child: Image(image: NetworkImage(gambar),),
          ),
        ),
    );
  }
}