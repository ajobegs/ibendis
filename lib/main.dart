import 'package:flutter/material.dart';
// import 'package:http/http.dart' as http;
// import 'dart:async';
// import 'dart:convert';
// import 'package:ihendis/hal_admin.dart';
// import 'package:ihendis/hal_endrosement.dart';
// import 'package:ihendis/hal_medical.dart';
// import 'package:ihendis/hal_training.dart';

///Tutorial 8
void main() {
  runApp(new MaterialApp(
    title: 'Supa Hero List',
    home: new Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Container> daftarSupahero = List();

  var karakter = [
    {"nama": "Baby", "gambar": "baby.jpg"},
    {"nama": "Doraemon", "gambar": "doraemon.jpg"},
    {"nama": "Dragon Ball", "gambar": "dragonboi.jpg"},
    {"nama": "Gavan", "gambar": "gavan.jpg"},
    {"nama": "Gundam", "gambar": "gundam.jpg"},
    {"nama": "Hentai Kamen", "gambar": "hentaikamen.jpg"},
    {"nama": "Kamen Rider Black", "gambar": "kamenrider.jpg"},
    {"nama": "Ultraman", "gambar": "ultraman.jpg"},
  ];

  _buatlist() async {
    for (var i = 0; i < karakter.length; i++) {
      final karakternya = karakter[i];
      final String gambar = karakternya['gambar'];
      daftarSupahero.add(Container(
        padding: EdgeInsets.all(8),
        child: Card(
          child: Column(
            children: <Widget>[
              Hero(
                tag: karakternya['nama'],
                child: Material(
                  child: InkWell(
                    onTap: () => Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => Detail(
                              nama: karakternya['nama'],
                              gambar: karakternya['gambar'],
                            ))),
                    child: Image.asset(
                      "assets/images/supahero/$gambar",
                      height: 100,
                      width: 200,
                      // color: Colors.redAccent,
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10.0),
              ),
              Text(
                karakternya['nama'],
                style: TextStyle(fontSize: 10),
              ),
            ],
          ),
        ),
      ));
    }
  }

  @override
  void initState() {
    _buatlist();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Side Bar - Drawer"),
        backgroundColor: Colors.purple,
      ),
      body: GridView.count(
        crossAxisCount: 2,
        children: daftarSupahero,
      ),
    );
  }
}

class Detail extends StatelessWidget {
  final String nama;
  final String gambar;

  const Detail({Key key, this.nama, this.gambar}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          Container(
              height: 240,
              child: Hero(
                tag: nama,
                child: Material(
                  child: InkWell(
                    child: Image.asset(
                      "assets/images/supahero/$gambar",
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              )),
          BahagianNama(
            nama: nama,
          ),
          BahagianIcon(),
          // Keterangan();
        ],
      ),
    );
  }
}

class BahagianNama extends StatelessWidget {
  final String nama;

  const BahagianNama({Key key, this.nama}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  nama,
                  style: TextStyle(fontSize: 20.0, color: Colors.blue),
                ),
                Text(
                  "$nama\@gmail.com",
                  style: TextStyle(fontSize: 17.0, color: Colors.grey),
                ),
              ],
            ),
          ),
          Row(
            children: <Widget>[
              Icon(
                Icons.star,
                size: 30,
                color: Colors.pink,
              ),
              Text('12', style: TextStyle(fontSize: 18),),
            ],
          )
        ],
      ),
    );
  }
}

class BahagianIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Iconteks(icon: Icons.call,teks: "Call",),
          Iconteks(icon: Icons.message,teks: "Email",),
          Iconteks(icon: Icons.link,teks: "Link",),
        ],
      ),
    );
  }
}

class Iconteks extends StatelessWidget {
  final IconData icon;
  final String teks;

  const Iconteks({Key key, this.icon, this.teks}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        children: <Widget>[
          Icon(icon, size: 50.0, color: Colors.blue,),
          Text(teks, style: TextStyle(fontSize: 18, color: Colors.blue),),
        ],
      ),
    );
  }
}

///Tutorial 7
// void main() {
//   runApp(new MaterialApp(
//     title: 'List View',
//     home: new Home(),
//   ));
// }

// class Home extends StatefulWidget {
//   @override
//   _HomeState createState() => _HomeState();
// }

// class _HomeState extends State<Home> {
//   String gambar1 =
//       'http://1.bp.blogspot.com/-zYhgjQem7qE/Txa35WTx_vI/AAAAAAAAALw/wM8GKcRFR1s/s1600/cute+funny+baby-cutebabiesphoto.blogspot.com+%25287%2529.jpg';
//   String gambar2 =
//       'https://www.rd.com/wp-content/uploads/2018/05/shutterstock_725437768-760x506.jpg';
//   String gambarBackup = '';

//   String nama1 = 'Paichow Da Great';
//   String name2 = 'Pok Noah';
//   String backupName = '';

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(
//           // title: Image.network('https://cdn.pixabay.com/photo/2016/06/17/06/04/background-1462755_960_720.jpg',
//           // fit: BoxFit.cover,
//           // ),
//           title: Text("Side Bar - Drawer"),
//           backgroundColor: Colors.purple,
//         ),
//         drawer: Drawer(
//           child: ListView(
//             children: <Widget>[
//               UserAccountsDrawerHeader(
//                 decoration: BoxDecoration(
//                     image: DecorationImage(
//                         fit: BoxFit.cover,
//                         image: NetworkImage(
//                             'https://cdn.pixabay.com/photo/2016/06/17/06/04/background-1462755_960_720.jpg'))),
//                 accountName: Text(nama1),
//                 accountEmail: Text('ajokontol@gmail.com'),
//                 currentAccountPicture: GestureDetector(
//                   //cara singkat
//                   // onTap: ()=> Navigator.of(context).push(MaterialPageRoute(
//                   //   builder: (BuildContext context)=> UserDetails(nama: nama1,gambar: gambar1,)
//                   // )),
//                   //cara lama
//                   onTap: () {
//                     Navigator.pop(context);
//                     Navigator.of(context).push(MaterialPageRoute(
//                         builder: (BuildContext context) => UserDetails(
//                               nama: nama1,
//                               gambar: gambar1,
//                             )));
//                   },

//                   child: CircleAvatar(
//                     backgroundImage: NetworkImage(gambar1),
//                   ),
//                 ),
//                 otherAccountsPictures: <Widget>[
//                   GestureDetector(
//                     onTap: () => gantiUser(),
//                     child: CircleAvatar(
//                       backgroundImage: NetworkImage(gambar2),
//                     ),
//                   ),
//                 ],
//               ),
//               ListTile(
//                 trailing: Icon(Icons.home),
//                 title: Text("Home"),
//               ),
//               ListTile(
//                 title: Text("Settings"),
//                 trailing: Icon(Icons.settings),
//               ),
//             ],
//           ),
//         ),
//         body: Container());
//   }

//   void gantiUser() {
//     this.setState(() {
//       gambarBackup = gambar1;
//       gambar1 = gambar2;
//       gambar2 = gambarBackup;

//       backupName = nama1;
//       nama1 = name2;
//       name2 = backupName;
//     });
//   }
// }

///Tutorial 7
// void main() {
//   runApp(new MaterialApp(
//     title: 'List View',
//     home: new Home(),
//   ));
// }

// class Home extends StatefulWidget {
//   @override
//   _HomeState createState() => _HomeState();
// }

// class _HomeState extends State<Home> {
//   String teks = 'empty';

//   TextEditingController controllerInput = new TextEditingController();
//   TextEditingController controllerAlert = new TextEditingController();
//   TextEditingController controllerSnackbar = new TextEditingController();

//   final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       key: _scaffoldState,
//       appBar: AppBar(
//         title: Text("Input text, alert dialog & SnackPlate"),
//         backgroundColor: Colors.brown,
//       ),
//       body: Container(
//         child: Column(
//           children: <Widget>[
//             TextField(
//               controller: controllerInput,
//               decoration: InputDecoration(hintText: 'fill in'),
//               // onChanged: (String str) {
//               onSubmitted: (String str) {
//                 setState(() {
//                   teks = str + '\n' + teks;
//                   controllerInput.text = 'boo';
//                 });
//               },
//             ),
//             Text(
//               teks,
//               style: TextStyle(fontSize: 20, color: Colors.green),
//             ),
//             TextField(
//               controller: controllerAlert,
//               decoration: InputDecoration(hintText: 'fill in'),
//               // onChanged: (String str) {
//               onSubmitted: (String str) {
//                 controllerAlert.text = '';
//                 _alertdialog(str);
//               },
//             ),
//             TextField(
//               controller: controllerSnackbar,
//               decoration: InputDecoration(hintText: 'fill in'),
//               // onChanged: (String str) {
//               onSubmitted: (String str) {
//                 controllerSnackbar.text = '';
//                 _snakbar(str);
//               },
//             ),
//           ],
//         ),
//       ),
//     );
//   }

//   void _alertdialog(String str) {
//     if (str.isEmpty) {
//       return;
//     }
//     AlertDialog alertDialog = AlertDialog(
//       content: Text(str, style: TextStyle(color: Colors.grey, fontSize: 20.0)),
//       actions: <Widget>[
//         RaisedButton(
//           color: Colors.green,
//           child: Text(
//             'Ok',
//             style: TextStyle(color: Colors.white),
//           ),
//           onPressed: () {
//             Navigator.pop(context);
//           },
//         ),
//       ],
//     );
//     showDialog(
//         context: context,
//         builder: (BuildContext context) {
//           return alertDialog;
//         });
//   }

//   void _snakbar(String str) {
//     if (str.isEmpty) {
//       return;
//     }

//     _scaffoldState.currentState.showSnackBar(SnackBar(
//       content: Text(
//         str,
//         style: TextStyle(fontSize: 20.0),
//       ),
//       duration: Duration(seconds: 1),
//     ));
//   }
// }

///Tutorial 6.1
// void main() {
//   runApp(new MaterialApp(
//     title: 'List View',
//     home: new Home(),
//   ));
// }

// class Home extends StatefulWidget {
//   @override
//   _HomeState createState() => _HomeState();
// }

// class _HomeState extends State<Home> {
//   List dataJSON;

//   Future<String> ambikDataJSON() async {
//     http.Response hasil =
//         await http.get('https://jsonplaceholder.typicode.com/posts');
//     // await http.get(
//     //     Uri.encodeFull('https://jsonplaceholder.typicode.com/posts'),
//     //     headers: {'Accept': 'application/json'});

//     this.setState(() {
//       dataJSON = json.decode(hasil.body);
//     });
//   }

//   @override
//   void initState() {
//     this.ambikDataJSON();
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("List view JSON"),
//       ),
//       body: ListView.builder(
//         itemCount: dataJSON == null ? 0 : dataJSON.length,
//         itemBuilder: (context, int index) {
//           return Container(
//             padding: EdgeInsets.all(2),
//             child: Card(
//               child: Container(
//                  padding: EdgeInsets.all(10),
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: <Widget>[
//                     Text(
//                       dataJSON[index]['title'],
//                       style: TextStyle(color: Colors.blue, fontSize: 20.0),
//                     ),
//                     Text(dataJSON[index]['body'])
//                   ],
//                 ),
//               ),
//             ),
//           );
//         },
//       ),
//     );
//   }
// }

///Tutorial 6
// void main() {
//   runApp(new MaterialApp(
//     title: 'List View',
//     home: new Home(listData: List<String>.generate(100, (i)=>'ini data ke $i'),),
//   ));
// }

// class Home extends StatelessWidget {

// final List<String> listData;

// Home({Key key, this.listData}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(title: Text('List JSON Data'),backgroundColor: Colors.blueGrey,),

//       body: Container(
//         child: ListView.builder(
//           itemBuilder: (context, int index) {
//             return ListTile(
//               leading: Icon(Icons.widgets),
//               title: Text('${listData[index]}'),
//             );
//           },
//         itemCount: listData.length,
//         ),
//       ),
//     );
//   }
// }

///Tutorial 5
// void main() {
//   runApp(new MaterialApp(
//     title: 'List View',
//     home: new Home(),
//   ));
// }

// class Home extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.teal[500],
//         title: Text('Daftar Tutorial List'),
//       ),
//       body: ListView(
//         children: <Widget>[
//           ListDaftar(
//             gambar: 'assets/images/baby.jpg',
//             description: 'FOR SALE RM5/-',
//           ),
//           ListDaftar(
//             gambar: 'assets/images/papa.jpg',
//             description: 'FOR SALE RM 10/-',
//           ),
//           ListDaftar(
//             gambar: 'assets/images/baby.jpg',
//             description: 'FOR SALE RM5/-',
//           ),
//           ListDaftar(
//             gambar: 'assets/images/papa.jpg',
//             description: 'FOR SALE RM 10/-',
//           ),
//           ListDaftar(
//             gambar: 'assets/images/baby.jpg',
//             description: 'FOR SALE RM5/-',
//           ),
//           ListDaftar(
//             gambar: 'assets/images/papa.jpg',
//             description: 'FOR SALE RM 10/-',
//           ),
//           ListDaftar(
//             gambar: 'assets/images/baby.jpg',
//             description: 'FOR SALE RM5/-',
//           ),
//           ListDaftar(
//             gambar: 'assets/images/papa.jpg',
//             description: 'FOR SALE RM 10/-',
//           ),
//         ],
//       ),
//     );
//   }
// }

// class ListDaftar extends StatelessWidget {
//   final String gambar;
//   final String description;

//   const ListDaftar({Key key, this.gambar, this.description}) : super(key: key);
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: EdgeInsets.all(20),
//       child: Row(
//         children: <Widget>[
//           Image(
//             image: AssetImage(gambar),
//             width: 150.0,
//           ),
//           Container(
//             padding: EdgeInsets.all(5),
//             child: Center(
//               child: Column(
//                 children: <Widget>[
//                   Text(description, style: TextStyle(fontSize: 20)),
//                   Text('Menang 10x pak!',
//                       style: TextStyle(
//                         fontSize: 15,
//                         color: Colors.grey,
//                       ))
//                 ],
//               ),
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }

/// Tutorial 4
// void main() {
//   runApp(new MaterialApp(
//     title: 'iHENDIS APPS',
//     home: new Home(),
//   ));
// }

// class Home extends StatefulWidget {
//   @override
//   _HomeState createState() => _HomeState();
// }

// class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
//   TabController controller;

//   @override
//   void initState() {
//     controller = TabController(vsync: this, length: 4);
//     super.initState();
//   }

//   @override
//   void dispose() {
//     controller.dispose();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.purple,
//         title: Text('Daftar Online'),
//         bottom: TabBar(
//           controller: controller,
//           tabs: <Widget>[
//             Tab(
//               icon: Icon(Icons.home),
//               text: 'Admin',
//             ),
//             Tab(
//               icon: Icon(Icons.restaurant_menu),
//               text: 'Endorsement',
//             ),
//             Tab(
//               icon: Icon(Icons.directions_boat),
//               text: 'Medical',
//             ),
//             Tab(
//               icon: Icon(Icons.train),
//               text: 'Training',
//             ),
//           ],
//         ),
//       ),
//       body: TabBarView(
//         controller: controller,
//         children: <Widget>[Admin(), Endorse(), Medical(), Training()],
//       ),
//       bottomNavigationBar: Material(
//         color: Colors.indigo,
//         child: TabBar(
//           controller: controller,
//           tabs: <Widget>[
//             Tab(icon: Icon(Icons.mic)),
//             Tab(icon: Icon(Icons.restaurant_menu)),
//             Tab(icon: Icon(Icons.directions_boat)),
//             Tab(icon: Icon(Icons.train)),
//           ],
//         ),
//       ),
//     );
//   }
// }
// Tutorial 3
// void main() {
//   runApp(new MaterialApp(
//     title: 'iHENDIS APPS',
//     home: new Page1(),
//     routes: <String,WidgetBuilder>{
//       '/page1' : (BuildContext context) => Page1(),
//       '/page2' : (BuildContext context) => Page2(),
//       '/page3' : (BuildContext context) => Page3(),
//     },
//   ));
// }
// class Page1 extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         // leading: Icon(Icons.check),
//         title: Text("SPORT"),
//       ),
//       body: Center(
//         child: IconButton(
//           icon: Icon(
//             Icons.directions_run,
//             size: 70,
//             color: Colors.grey,
//           ),
//           onPressed: (){
//             Navigator.pushNamed(context, '/page2');
//           },
//         ),
//       ),
//     );
//   }
// }

// class Page2 extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         // leading: Icon(Icons.check),
//         title: Text("MUSIC"),
//       ),
//       body: Center(
//         child: IconButton(
//           icon: Icon(
//             Icons.speaker,
//             size: 70,
//             color: Colors.pink,
//           ),
//           onPressed: (){
//             Navigator.pushNamed(context, '/page3');
//           },
//         ),
//       ),
//     );
//   }
// }
// /// Tutorial 2
// class Page3 extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         // leading: Icon(Icons.check),
//         title: Text("Card and Parsing data"),
//       ),
//       body: Container(
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.stretch,
//           children: <Widget>[
//             MyCard(ikon: Icons.home, teks: 'Home',warnaIkon: Colors.lime,),
//             MyCard(ikon: Icons.chat, teks: 'Chat',warnaIkon: Colors.amber,),
//             MyCard(ikon: Icons.note, teks: 'Data Entry',warnaIkon: Colors.orange,),
//             MyCard(ikon: Icons.settings, teks: 'settings',warnaIkon: Colors.red,),
//           ],
//         ),
//       ),
//     );
//   }
// }

// class MyCard extends StatelessWidget {
//   final IconData ikon;
//   final String teks;
//   final Color warnaIkon;

//   MyCard({Key key, this.ikon, this.teks, this.warnaIkon}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: EdgeInsets.all(4.0),
//       child: Card(
//           child: Column(
//         children: <Widget>[
//           Icon(
//             ikon,
//             size: 70,
//             color: warnaIkon,
//           ),
//           Text(
//             teks,
//             style: TextStyle(fontSize: 20),
//           ),
//         ],
//       )),
//     );
//   }
// }

/// Tutorial 1
// class IndexMain extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.grey[50],
//       appBar: AppBar(
//         leading: Icon(Icons.home),
//         backgroundColor: Colors.green,
//         title: Center(child: Text('IHENDIS')),
//         actions: <Widget>[Icon(Icons.search)],
//       ),
//       body: Container(
//           child: Column(
//         children: <Widget>[
//           Row(
//             children: <Widget>[
//               Icon(
//                 Icons.settings,
//                 size: 80,
//                 color: Colors.pink,
//               ),
//               Icon(
//                 Icons.home,
//                 size: 80,
//                 color: Colors.pink,
//               ),
//               Column(
//                 children: <Widget>[
//                   Icon(
//                     Icons.cake,
//                     size: 80,
//                     color: Colors.pink,
//                   ),
//                   Icon(
//                     Icons.priority_high,
//                     size: 80,
//                     color: Colors.pink,
//                   ),
//                 ],
//               )
//             ],
//           ),
//           Icon(
//             Icons.face,
//             size: 80,
//             color: Colors.pink,
//           ),
//           Icon(
//             Icons.dashboard,
//             size: 80,
//             color: Colors.pink,
//           ),
//         ],
//       )),
//       // Center(
//       //   child: Container(
//       //     color: Colors.green[900],
//       //     width: 200.0,
//       //     height: 100.0,
//       //     child: Center(
//       //       child:
//       //       Icon(Icons.android,color: Colors.white,size: 70,
//       //       // Text(
//       //       //   'Center text',
//       //       //   style: TextStyle(
//       //       //       color: Colors.white, fontFamily: "Serif", fontSize: 20),
//       //       // ),
//       //     ),
//       //   ),
//       // ),
//       // ),
//     );
//   }
// }
