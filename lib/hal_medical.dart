import 'package:flutter/material.dart';

class Medical extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          children: <Widget>[
            Padding(padding: EdgeInsets.all(20),),
            Text('MEDICAL', style: TextStyle(fontSize: 30)),
            Padding(padding: EdgeInsets.all(20),),
            // Icon(Icons.directions_boat, size: 100.0,color: Colors.grey,),
            Image(image: NetworkImage("https://mybroadband.co.za/news/wp-content/uploads/2016/10/Medical-aid.jpg"),width: 200.0,),
          ],
        ),
      ),
    );
  }
}